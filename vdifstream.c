////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////
//
//   VDIF test data stream generator v1.1
//   (C) 2020 Jan Wagner
//
////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////
//
// $ vdifstream [--psn32/64] [--dbg] [--station=ID] [--thread=n] [--log2nchan=n]
//              [--refepoch=n] [--now] [--loop|-l] <ipaddr> <port> <Mbps> [<input.vdif>]
//
// Sends UDP/IP-encapsulated 1312-byte VDIF data frames.
//
// The VDIF can be generated in realtime with fake timestamps and data.
//
// The VDIF can also be read from a file with 1312-byte VDIF frames, with
// true timestamps and data taken from the file.
//
// In front of every VDIF frame a 32-bit or 64-bit Packet Sequence Number (PSN)
// can be added using the --psn32 or --psn64 options. This increases the
// size of the UDP/IP packet to 1316 byte or to 1320 byte.
//
// All 32-bit words in the VDIF frame are in Little Endian format.
//   http://vlbi.org/vdif/docs/VDIF_specification_Release_1.1.1.pdf
// The extra PSN in front of VDIF can be in Little or Big Endian format.
//
////////////////////////////////////////////////////////////////////////////////////

/*
  $ gcc -Wall -O3 -msse3 -D_FILE_OFFSET_BITS=64 -D_LARGEFILE_SOURCE \
        -D_LARGEFILE64_SOURCE vdifstream.c -o vdifstream
*/

#define PSN_IS_LITTLE_ENDIAN    1 // 1 for little-Endian Packet Sequence Number, 0 for big-Endian
#define VDIF_HEADER_LEN        32
#define VDIF_PAYLOAD_LEN     1280 // 1280, 8000

#define DEBUG_FILE_NAME      "vdifstream_dbg.vdif"

#include <sys/stat.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <sys/fcntl.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <getopt.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <memory.h>
#include <malloc.h>
#include <string.h>

#ifndef _BSD_SOURCE
#define _BSD_SOURCE
#endif
#include <endian.h>

void usage(void)
{
   printf("\n"
          " A utility to send 1312-byte VDIF data frames in UDP/IP packets\n"
          " (C) 2010 Jan Wagner\n\n"
          " Usage: vdifstream [--psn32/64] [--loop] [--dbg] [--iface|i=ethXX]\n"
          "                   [--ttl|T=n] [--station|s=ID] [--thread|t=n]\n"
          "                   [--log2nchan|c=n] [--refepoch|e=n] [--now] [--size|z=n]\n"
          "                   <ipaddr> <port> <Mbps> [<input.vdif>]\n\n"
          "   Options: for UDP generation\n"
          "     --psn32    : enable 32-bit packet sequence numbers, or\n"
          "     --psn64    : enable 64-bit packet sequence numbers\n"
          "     --loop     : if input file is used then restart at EOF\n"
          "     --dbg      : write the UDP packets to %s\n"
          "     --iface    : specify name of interface to send on\n"
          "     --ttl      : set packet TTL (0:local, 1:subnet (default), 2:wider)\n\n"
          "   Options: for fake VDIF generation\n"
          "     --station  : 2-letter station ID\n"
          "     --thread   : thread number\n"
          "     --log2nchan: number of channels or IFs\n"
          "     --refepoch : reference epoch\n"
          "     --now      : use system time for epoch and first data-second\n"
          "     --size     : payload size in bytes\n\n"
          "   Arguments:\n"
          "     ipaddr     : destination IP address (normal or multicast)\n"
          "                  like 127.0.0.1 or 224.0.0.1\n"
          "     port       : destination IP port number\n"
          "     Mbps       : rate in Megabit/second for the raw VLBI data\n"
          "     input.vdif : optional input file, if the file should be\n"
          "                  transferred instead of fake VDIF data\n"
          "\n"
          " Notes:\n"
          " 1) due to protocol overhead, 1312-byte payload at 1024 Mbps\n"
          "    will produce 1024*(1312+8)/1280 = 1056 Mbps UDP traffic\n"
          " 2) payload size in <input.vdif> and --size=n must match\n"
          " 3) without an input file the program generates 8-bit data\n"
          " 4) default TTL is 1 (same subnet) to reduce flooding risk\n",
          DEBUG_FILE_NAME
    );
}

static struct option long_options[] =
    {
        {"dbg",       no_argument, 0, 'd'},
        {"psn32",     no_argument, 0, 'p'},
        {"psn64",     no_argument, 0, 'P'},
        {"loop",      no_argument, 0, 'l'},
        {"now",       no_argument, 0, 'N'},
        {"station",   required_argument, 0, 's'},
        {"thread",    required_argument, 0, 't'},
        {"log2nchan", required_argument, 0, 'c'},
        {"refepoch",  required_argument, 0, 'e'},
        {"size",      required_argument, 0, 'z'},
        {"iface",     required_argument, 0, 'i'},
        {"ttl",       required_argument, 0, 'T'},
        {0, 0, 0, 0}
    };


void tm_to_vdif_time(const struct tm* tm, int* vdifepoch, uint32_t* frame_sec)
{
    // struct tm::tm_year ref is year 1900 ie 0 = year 1900, 100 = year 2000
    // struct tm::tm_mon  range is 0..11
    // struct tm::tm_yday range is 0..365 (leap) or 0..364 (normal year)
    // vdif ref.ep. 0 is 01/01/2000, ref.ep.1 is 01/07/2000, ref.ep. 2 is 01/01/2001, ...
    // note: 01july has doy 182, or doy 181 in leap-year

    *vdifepoch = 2*(tm->tm_year - 100);
    *frame_sec = 86400 * (uint32_t)tm->tm_yday;
    *frame_sec += 3600*tm->tm_hour + 60*tm->tm_min + tm->tm_sec;

    if (tm->tm_mon >= 6)
    {
        int year = 1900 + tm->tm_year;
        if ((year % 400 == 0 || year % 4 == 0) && !(year % 100 == 0)) // leap yr
        {
            *frame_sec = *frame_sec - 182*86400;
        }
        else
        {
            // *frame_sec = *frame_sec - 183*86400;
            *frame_sec = *frame_sec - 181*86400;
        }
        *vdifepoch = *vdifepoch + 1;
    }
}


int main(int argc, char** argv)
{
   struct tm utc_pc;
   time_t now;
   int c;
   int sd, fdi = 0, fdo = 0;

   /* Args */
   char*  target_ip;
   int    target_port;
   double rate_Mbps = 1024;
   char*  infilename = NULL;

   /* Defaults */
   int use_psn32   = 0;
   int use_psn64   = 0;
   int use_infinite_loop = 0;
   int use_infile  = 0;
   int use_outfile = 0;
   int use_systime = 0;
   int udp_psn_len = 0;
   int thread_id   = 0;
   int refepoch    = 12;
   int log2nchan   = 4;
   char* station_id = NULL;
   char* iface = NULL;
   int packet_ttl = 1; // 0:same host, 1:same subnet, 2:wider, 32:entire site

   /* Data frame buffer */
   unsigned char*  udp_frame;
   unsigned char*  vdif_frame;
   uint32_t*       udp_psn32;
   uint64_t*       udp_psn64;
   uint32_t*       vdif_w0;
   uint32_t*       vdif_w1;
   uint32_t*       vdif_w2;
   uint32_t*       vdif_w3;

   /* Network */
   struct sockaddr_in si_remote;
   struct timeval tv_start;
   struct timeval tv_curr;
   struct timeval tv_old;
   signed long    ipd_delta = 0;
   signed long    ipd_accu = 0;
   useconds_t     ipd_target_usec;
   double         rate_Mbps_with_overheads;

   uint64_t psn64     = 0;
   uint32_t frame_sec = 0;
   uint32_t frame_nr  = 0;
   int      rate_fps  = 0;
   size_t   udp_size  = 0;
   size_t   pld_size  = VDIF_PAYLOAD_LEN;
   size_t   frame_size= VDIF_HEADER_LEN + pld_size;

   /* Command Line Arguments */
   while (1)
   {
      int option_index;
      c = getopt_long(argc, argv, "dpPls:c:t:e:z:", long_options, &option_index);
      if (c == -1) { break; }

      switch (c)
      {
           case 'd':
              use_outfile = 1;
              break;
           case 'p':
              use_psn32 = 1;
              use_psn64 = 0;
              udp_psn_len = 4;
              break;
           case 'P':
              use_psn32 = 0;
              use_psn64 = 1;
              udp_psn_len = 8;
              break;
           case 'l':
              use_infinite_loop = 1;
              break;
           case 's':
              station_id = strndup(optarg, 2);
              break;
           case 'c':
              log2nchan = atoi(optarg);
              break;
           case 't':
              thread_id = atoi(optarg);
              break;
           case 'e':
              refepoch = atoi(optarg) & 0x1F;
              break;
           case 'N':
              use_systime = 1;
              break;
           case 'z':
              pld_size = atoi(optarg);
              break;
           case 'i':
              iface = strdup(optarg);
              break;
           case 'T':
              packet_ttl = atoi(optarg);
              break;
           default:
              usage();
              return -1;
      }
   }

   // Main args:  ip   port   Mbps
   if ((argc - optind) < 3)
   {
       usage();
       return -1;
   }
   target_ip   = argv[optind++];
   target_port = atoi(argv[optind++]);
   rate_Mbps   = atof(argv[optind++]);

   // Optional: [<infile.vdif>]
   if ((argc - optind) > 0)
   {
      use_infile = 1;
      infilename = argv[optind++];
      fdi = open(infilename, O_RDONLY|O_LARGEFILE);
      if (fdi < 0)
      {
         fprintf(stderr, "Could not open input file: %s\n", infilename);
         return -1;
      }
   }

   // Debug output
   if (use_outfile)
   {
      fdo = creat(DEBUG_FILE_NAME, S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);
   }


   /* Allocate buffer for single UDP frame with VDIF */
   udp_size = VDIF_HEADER_LEN + pld_size;
   if (use_psn32 || use_psn64)
   {
      udp_size += udp_psn_len;
   }
   udp_frame  = (unsigned char*)memalign(128, udp_size);
   udp_psn32  = (uint32_t*)udp_frame;
   udp_psn64  = (uint64_t*)udp_frame;
   vdif_frame = udp_frame;
   if (use_psn32 || use_psn64)
   {
      vdif_frame += udp_psn_len;
   }
   vdif_w0 = (uint32_t*)(vdif_frame + 0);
   vdif_w1 = (uint32_t*)(vdif_frame + 4);
   vdif_w2 = (uint32_t*)(vdif_frame + 8);
   vdif_w3 = (uint32_t*)(vdif_frame + 12);

   memset(udp_frame, 0x00, udp_size);

   /* Prepare standard fields of VDIF header */
   //vdif_frame[0+2*4] = 0xA4; // length in 8-byte units = 1312/8
   //vdif_frame[1+2*4] = 0x00; // = 164 = 0x00A4
   frame_size = VDIF_HEADER_LEN + pld_size;
   vdif_frame[0+2*4] = (frame_size/8) & 0xFF;
   vdif_frame[1+2*4] = ((frame_size/8) >> 8) & 0xFF;
   vdif_frame[3+2*4] = (vdif_frame[3+2*4] & ~0x1F) | (log2nchan & 0x1F);
   if (station_id != NULL)
   {
       vdif_frame[0+3*4] = station_id[1];
       vdif_frame[1+3*4] = station_id[0];
   }
   vdif_frame[2+3*4] = thread_id & 0x00FF; // Thread ID field is 10-bit, but for now we just discard the upper 2 bits...
   vdif_frame[3+3*4] |= 0x04;              // Bits per sample minus 1 in bits 2:7 (4>>2 = 1 = "2-bit samples")

   /* Data rate, packets per second, microseconds per packet */
   rate_Mbps_with_overheads = rate_Mbps * ((double)udp_size) / ((double)pld_size);
   ipd_target_usec = 1e6 * ((double)pld_size) / (rate_Mbps*1e6 / 8.0);
   rate_fps        = (rate_Mbps*1e6/8) / ((double)pld_size);

   /* Transmission socket */
   sd = socket(PF_INET, SOCK_DGRAM, 0);
   si_remote.sin_family = AF_INET;
   si_remote.sin_port = htons(target_port);
   if (1 != inet_pton(AF_INET, target_ip, &si_remote.sin_addr)) {
      printf("Target IP address parsing failed\n");
      return -1;
   }
   int loop = 1;
   int reuse = 1;
   setsockopt(sd, IPPROTO_IP, IP_MULTICAST_LOOP, &loop, sizeof(loop));
   setsockopt(sd, IPPROTO_IP, IP_MULTICAST_TTL, &packet_ttl, sizeof(packet_ttl));
   setsockopt(sd, SOL_SOCKET, SO_REUSEADDR, &reuse, sizeof(reuse));
   setsockopt(sd, SOL_SOCKET, SO_NO_CHECK, &reuse, sizeof(reuse));

   /* Restrict to an interface */
   // IP_MULTICAST_IF takes struct in_addr interface_addr
   // SO_BINDTODEVICE takes device name
   if (iface != NULL)
   {
       printf("Restricting to iface : %s\n", iface);
       if (setsockopt(sd, SOL_SOCKET, SO_BINDTODEVICE, iface, strlen(iface)) < 0)
       {
           perror("setsockopt(SO_BINDTODEVICE)");
           exit(EXIT_FAILURE);
       }
   }

   /* Summary */
   if (use_infile)
   {
      printf("Input file           : %s (make sure its format matches the VDIF size below!)\n", infilename);
   }
   else
   {
       printf("Fake data            : generating VDIF with 8-bit count data\n");
   }
   if (use_outfile)
   {
      printf("Debug copy           : %s will contain all sent UDP packets\n", DEBUG_FILE_NAME);
   }
   printf("Sending rate         : %f Mbps = %.2f Mbps with overheads\n", rate_Mbps, rate_Mbps_with_overheads);
   printf("Packet rate          : %d usec IPD, %d packets per second\n", ipd_target_usec, rate_fps);
   printf("Output VDIF size     : %zu bytes, %d header + %zu payload\n",
          VDIF_HEADER_LEN+pld_size, VDIF_HEADER_LEN, pld_size);
   if (use_psn32 || use_psn64)
   {
      printf("Packet sequence nrs  : %d-bit in %s Endian byte order\n",
             8*udp_psn_len, (PSN_IS_LITTLE_ENDIAN ? "Little" : "Big") );
   }
   printf("Final UDP packet size: %d byte\n", (int)udp_size);


   /* Wait for integer 1-sec roll over */
   gettimeofday(&tv_old, NULL);
   do
   {
      gettimeofday(&tv_start, NULL);
   } while(tv_old.tv_sec == tv_start.tv_sec);

   /* Set VDIF timestamps to start from current time */
   if (use_systime)
   {
      time(&now);
      gmtime_r(&now, &utc_pc);
      tm_to_vdif_time(&utc_pc, &refepoch, &frame_sec);
      printf("Based on current UTC time derived VDIF ref.epoch of %d, second of %u\n", refepoch, frame_sec);
   }

   /* Sending loop */
restart:
   gettimeofday(&tv_old, NULL);
   while (1)
   {
      ssize_t n;

      /* packet spacing pre-adjust [usec] */
      gettimeofday(&tv_curr, NULL);
      // ipd_delta = ipd_target_usec - ((tv_curr.tv_sec-tv_old.tv_sec)*1e6+(tv_curr.tv_usec-tv_old.tv_usec)); // IPD without absolute time tie
      ipd_delta = (psn64+1)*ipd_target_usec - ((tv_curr.tv_sec-tv_start.tv_sec)*1e6+(tv_curr.tv_usec-tv_start.tv_usec)); // IPD on a static time grid since start
      if ((ipd_delta > 0) || (ipd_accu > 0)) {
         ipd_accu = ipd_accu + ipd_delta;
      }
      tv_old = tv_curr;

      /* Make new UDP frame */
      if (use_psn64)
      {
         #if PSN_IS_LITTLE_ENDIAN
         *udp_psn64 = htole64(psn64);
         #else
         *udp_psn64 = htobe64(psn64);
         #endif
      }
      else if(use_psn32)
      {
         #if PSN_IS_LITTLE_ENDIAN
         *udp_psn32 = htole32((uint32_t)psn64);
         #else
         *udp_psn32 = htobe32((uint32_t)psn64);
         #endif
      }
      if (use_infile)
      {
         // Read VDIF frame
         n = read(fdi, (void*)vdif_frame, VDIF_HEADER_LEN+pld_size);
         if (n < VDIF_HEADER_LEN+pld_size)
         {
             printf("Reached end of file.\n");
             if (use_infinite_loop)
             {
                 lseek(fdi, 0, SEEK_SET);
                 goto restart;
             }
             break;
         }
         // Decode timestamp
         frame_sec = le32toh((*vdif_w0) & 0x3FFFFFFF);
         frame_nr  = le32toh((*vdif_w1) & 0x00FFFFFF);
      }
      else
      {
         // Create VDIF data
         memset(vdif_frame+VDIF_HEADER_LEN, (psn64 & 0xFF), pld_size);
         // Set timestamp
         *vdif_w0 = htole32(frame_sec & 0x3FFFFFFF);
         *vdif_w1 = htole32((frame_nr & 0x00FFFFFF) | (refepoch << 24));
      }

      /* Output the UDP packet */
      n = sendto(sd, udp_frame, udp_size, 0, (struct sockaddr *)&si_remote, sizeof(struct sockaddr));
      if (n < udp_size) {
          printf("send err\n");
      }
      if (use_outfile)
      {
         write(fdo, (void*)udp_frame, udp_size);
      }

      /* Report? */
      if (frame_nr == 0)
      {
         printf("%2s data second %u, psn %lu\n", station_id, frame_sec, psn64);
      }

      /* Next PSN and timestamp */
      ++psn64;
      ++frame_nr;
      if (frame_nr >= rate_fps)
      {
         frame_nr = 0;
         ++frame_sec;
      }

      /* Throttle */
      if (ipd_accu > 0)
      {
         usleep(ipd_accu);
      }
   }

   return 0;
}


