Programs:

   vdifstream      -- send UDP/IP stream of VDIF frames
   vdiftimeUDP     -- real-time VDIF (in UDP/IP) timestamp to UTC comparison
   vdifsnapshotUDP -- capture UDP/IP with VDIF into memory first (some GB), then write to file
   vdifcontinuitycheck.py -- similar to vdiftimeUDP but for a file

--------------------------------------------------------------------------------
vdifstream
--------------------------------------------------------------------------------

   VDIF test data stream generator v1.0
   (C) 2014 Jan Wagner

 Intended for radio interferometry (VLBI), a tool to generate a stream of UDP/IP  
 packets that contain data in the VDIF format (http://vlbi.org/vdif/). The data
 can be generated on the fly (test data) or can be read from a VDIF formatted
 existing file.


Compile and install:

 $ make ; sudo make install

Usage:

 $ vdifstream [--psn32/64] [--dbg] [--station=ID] [--thread=n] [--log2nchan=n]
              [--refepoch=n] <ipaddr> <port> <Mbps> [<input.vdif>]

 Call vdifstream without arguments to see help.

Description:

 Sends UDP/IP-encapsulated 1312-byte VDIF data frames.

 The VDIF can be generated in realtime with fake timestamps and data.

 The VDIF can also be read from a file with 1312-byte VDIF frames, with
 true timestamps and data taken from the file.

 In front of every VDIF frame a 32-bit or 64-bit Packet Sequence Number (PSN)
 can be added using the --psn32 or --psn64 options. This increases the
 size of the UDP/IP packet to 1316 byte or to 1320 byte.

 All 32-bit words in the VDIF frame are in Little Endian format.
   http://vlbi.org/vdif/docs/VDIF_specification_Release_1.1.1.pdf
 The extra PSN in front of VDIF can be in Little or Big Endian format.




--------------------------------------------------------------------------------
vdiftimeUDP
--------------------------------------------------------------------------------

Usage: 

  $ vdiftimeUDP [--offset=n] [--bigendian] [--saveto=filename] [--cpu=n] <port>

  --offset=n  : remove the first n bytes in the UDP packet (e.g., remove 8-byte PSN)
  --bigendian : specify if data are VDIF-like but with Big Endian ordered headers
  --saveto=fn : at every 1-second change, store the single VDIF frame into given file
  --cpu=n     : tie the program to CPU core n (0-31), can reduce packet loss
  port        : port to listen on for UDP


Decodes the time stamp of incoming UDP data with VDIF and an possible packet
sequence number. The packet sequence number must be removed using --offset=.
The timestamp in the VDIF frame is printed every time it changes.
For reference the current computer time will be printed as well.

The VDIF vs. PC UTC time comparison can allow to check the
time synchronization of hardware that generates the UDP stream containing VDIF.

Note that the PC must be running NTP or PTP for such a comparison
to be meaningful.

If many UDP packets are lost by the program it is possible to use --cpu=2 (or 
other) to tie the program to a single CPU core. On a Mark6 this allows loss-free
reception and decoding at 8 Gbps.

More detail on VDIF headers can be inspected with the --saveto=<filename>
option. This writes the current VDIF frame into the file at the change of 
a data-second. The saved frame can be decoded with, e.g., vdifheader2.pl

Example program output is listed in  Examples-vdiftimeUDP.txt



--------------------------------------------------------------------------------
vdifsnapshotUDP
--------------------------------------------------------------------------------

Usage: 
 
   vdifsnapshotUDP <size in Mbyte> <port> <output file.vdif> [<skip n bytes in udp>]

A buffer of the specified size is allocated and UDP/IP to the specified port is
captured into this memory buffer. 
The capture process can strip the first N bytes, such as a packet sequence number.
After memory capture the data are written into the specified file, usually slowly.

Example: 

 $ ./vdifsnapshotUDP 2048 46227 /mnt/disks/cap2014y287d07h59m-8Gbps.snapshot.vdif 8

 Bound to CPU#2
 Capturing 2048 MByte into memory...
 ...................................................................
 ........... etc
 Memory capture ended, writing 2147418624 bytes to file...



--------------------------------------------------------------------------------
vdifcontinuitycheck.py
--------------------------------------------------------------------------------

Usage:

 vdifcontinuitycheck.py [-v] [--bigendian] [--framesize=<n bytes>] <filename>
   --v           : prints every frame
   --bigendian   : specify when data are VDIF-like but with wrong Endianness
   --framesize=n : specified frame size overrides the value in the VDIF headers

Example:

 $ vdifcontinuitycheck.py /data16/c172abc_tmp/Pv/c172b_PV_272-1400.vdif 
 Thread 0 Second 7826400 :  51200 frames : #0--#51199 : 0 lost, 0 out-of-order, 0 invalid, of 51200 total 
 Thread 0 Second 7826401 :  51200 frames : #0--#51199 : 0 lost, 0 out-of-order, 0 invalid, of 51200 total 
 Thread 0 Second 7826402 :  51200 frames : #0--#51199 : 0 lost, 0 out-of-order, 0 invalid, of 51200 total 
 Thread 0 Second 7826403 :  51200 frames : #0--#51199 : 0 lost, 0 out-of-order, 0 invalid, of 51200 total 
 Thread 0 Second 7826404 :  51200 frames : #0--#51199 : 0 lost, 0 out-of-order, 0 invalid, of 51200 total 
 Thread 0 Second 7826405 :  51200 frames : #0--#51199 : 0 lost, 0 out-of-order, 0 invalid, of 51200 total 

