////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////
//
//   VDIF UDP/IP Forwarding
//   Fallback in case iptables -j TEE and -j DNAT fail to accomplish the same.
//
//   (C) 2020 Jan Wagner
//
////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////
//
// $ vdifforwardUDP [--cpu=nr] [--mcast=group ip] [--miface=iface ip] [--addpsn64]
//                  <port> <dst_ip> [<dst_port>]
//
////////////////////////////////////////////////////////////////////////////////////

/*
  $ gcc -Wall -O3 -msse3 -D_FILE_OFFSET_BITS=64 -D_LARGEFILE_SOURCE \
        -D_LARGEFILE64_SOURCE -D_GNU_SOURCE vdifforwardUDP.c -o vdifforwardUDP
*/

// TODO: try recvmmsg() to receive multiple UDP in one call

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif
#include <arpa/inet.h>
#include <endian.h>
#include <errno.h>
#include <getopt.h>
#include <malloc.h>
#include <memory.h>
#include <netdb.h>
#include <netinet/in.h>
#include <sched.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/fcntl.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/types.h>
#include <time.h>
#include <unistd.h>

#ifndef _BSD_SOURCE
   #define _BSD_SOURCE
#endif
#include <endian.h>

#define MAX_RECEIVE_SIZE 65507 // max. expected UDP packet size containing a VDIF frame
#define RX_TIMEOUT_SEC   5     // timeout for listening to new UDP packets (is used to print a status)
#define VDIF_HEADER_SIZE 32
#define VDIF_LEGACY_HEADER_SIZE 16

static struct option long_options[] =
    {
        {"cpu",       required_argument, 0, 'c'},
	{"mcast",     required_argument, 0, 'm'},
	{"miface",    required_argument, 0, 'M'},
	{"addpsn64",  no_argument, 0, 'p'},
        {0, 0, 0, 0}
    };

void usage(void)
{
   printf("\n"
          "Usage: vdifforwardUDP [--cpu=nr] [--mcast=group ip] [--miface=iface ip] [--addpsn64] <port> <dst_ip> [<dst_port>]\n"
          "\n"
    );
}

void realtime_init(int cpu)
{
    // Bind to CPU that gave the best
    //   taskset -cp <cpu nr> <pid>
    //   e.g.  taskset -cp 2 3939
    cpu_set_t set;
    int rc;

    CPU_ZERO(&set);
    CPU_SET(cpu, &set);
    rc = sched_setaffinity(0, sizeof(set), &set);
    if (rc < 0)
    {
       printf("sched_setaffinity: could not set CPU affinity (maybe must run as root?)\n");
    }

    return;
}

void die(const char* fmt, const char* msg)
{
    printf(fmt, msg);
    printf("\n");
    exit(-1);
}


int main(int argc, char** argv)
{
   int sdi, sdo;
   const unsigned int yes = 1;

   /* Args */
   int c, numargs;
   int udp_port, dst_udp_port;
   char* target_ip_str;
   char* udp_port_str;
   char* mcast_group = NULL;
   char* mcast_iface = NULL;
   int do_add_psn64;

   /* Data frame buffer */
   unsigned char*  udp_frame;
   unsigned char*  udp_frame_rx;
   struct timeval  tv_prev, tv_curr, tv_timeout;
   uint64_t psn64 = 0;

   /* Network */
   struct sockaddr_storage src_addr;
   struct sockaddr_in si_remote;
   socklen_t src_addr_len = sizeof(src_addr);
   struct addrinfo  hints;
   struct addrinfo* res = 0;
   int loop = 1;
   int reuse = 1;
   int packet_ttl = 32; // 0:same host, 1:same subnet, 2:wider, 32:entire site

   size_t   framebytes_in_sec = 0;
   int      sec_count4printout = 0;

   /* Command Line Arguments */
   while (1)
   {
      int option_index;
      c = getopt_long(argc, argv, "c:m:M:p", long_options, &option_index);
      if (c == -1) { break; }

      switch (c)
      {
           case 'c':
              realtime_init(atoi(optarg));
              break;
           case 'm':
              mcast_group = strdup(optarg);
              break;
           case 'M':
              mcast_iface = strdup(optarg);
              break;
           case 'p':
              do_add_psn64 = 1;
              break;
           default:
              usage();
              return -1;
      }
   }

   // Main args:  <port> <dst ip> [<dst port>]
   numargs = argc - optind;
   if (numargs != 2 && numargs != 3)
   {
       usage();
       return -1;
   }
   udp_port_str  = strdup(argv[optind++]);
   target_ip_str = strdup(argv[optind++]);
   udp_port      = atoi(udp_port_str);
   if (numargs >= 3)
   {
      dst_udp_port = atoi(argv[optind++]);
   }
   else
   {
      dst_udp_port = udp_port;
   }
   printf("Receiving UDP on %d and sending to %s:%d %s adding 64-bit PSN\n", udp_port, target_ip_str, dst_udp_port, (do_add_psn64 ? "and" : "without"));

   /* Allocate buffer for single UDP frame with VDIF */
   udp_frame  = (unsigned char*)memalign(128, MAX_RECEIVE_SIZE);
   memset(udp_frame, 0x00, MAX_RECEIVE_SIZE);
   if (do_add_psn64)
   {
      udp_frame_rx = udp_frame + sizeof(uint64_t); // recv() into area after to-be-generated psn64
   }
   else
   {
      udp_frame_rx = udp_frame;
   }

   /* Receive socket */
   memset(&hints,0,sizeof(hints));
   hints.ai_family   = AF_UNSPEC;
   hints.ai_socktype = SOCK_DGRAM;
   hints.ai_protocol = 0;
   hints.ai_flags    = AI_PASSIVE | AI_ADDRCONFIG;
   getaddrinfo(NULL, udp_port_str, &hints, &res);
   //sd = socket(res->ai_family, res->ai_socktype, res->ai_protocol);
   sdi = socket(PF_INET, SOCK_DGRAM, 0);
   setsockopt(sdi, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(yes));
   if (bind(sdi, res->ai_addr, res->ai_addrlen) == -1)
   {
      die("%s", strerror(errno));
   }
   freeaddrinfo(res);
   if (mcast_group != NULL)
   {
      struct ip_mreq mreq;
      mreq.imr_multiaddr.s_addr = inet_addr(mcast_group);
      mreq.imr_interface.s_addr = htonl(INADDR_ANY);
      if (mcast_iface != NULL)
      {
         mreq.imr_interface.s_addr = inet_addr(mcast_iface);
      }
      setsockopt(sdi, IPPROTO_IP, IP_ADD_MEMBERSHIP, (char*) &mreq, sizeof(mreq));
   }
   tv_timeout.tv_sec  = RX_TIMEOUT_SEC;
   tv_timeout.tv_usec = 0;
   setsockopt(sdi, SOL_SOCKET, SO_RCVTIMEO, (char*)&tv_timeout, sizeof(struct timeval));
   c = 64*1024*1024;
   setsockopt(sdi, SOL_SOCKET, SO_RCVBUF, &c, sizeof(c));

   /* Sender socket */
   sdo = socket(PF_INET, SOCK_DGRAM, 0);
   si_remote.sin_family = AF_INET;
   si_remote.sin_port = htons(dst_udp_port);
   if (1 != inet_pton(AF_INET, target_ip_str, &si_remote.sin_addr)) {
      die("%s", "Target IP address parsing failed");
   }
   setsockopt(sdo, IPPROTO_IP, IP_MULTICAST_LOOP, &loop, sizeof(loop));
   setsockopt(sdo, IPPROTO_IP, IP_MULTICAST_TTL, &packet_ttl, sizeof(packet_ttl));
   setsockopt(sdo, SOL_SOCKET, SO_REUSEADDR, &reuse, sizeof(reuse));
   setsockopt(sdo, SOL_SOCKET, SO_NO_CHECK, &reuse, sizeof(reuse));

   /* Receiving loop */
   gettimeofday(&tv_prev, NULL);
   while (1)
   {
      ssize_t udp_size, nwr;

      udp_size = recvfrom(sdi, udp_frame_rx, MAX_RECEIVE_SIZE, 0, (struct sockaddr*)&src_addr, &src_addr_len);
      if (udp_size == -1)
      {
          if (errno == EAGAIN)
          {
              printf("No UDP packets received in the last %d seconds...\n", RX_TIMEOUT_SEC);
              continue;
          }
          else
          {
              die("%s", strerror(errno));
          }
      }

      if (udp_size < 32)
      {
         char host[NI_MAXHOST], service[NI_MAXSERV];
         int s = getnameinfo((struct sockaddr *)&src_addr,
                        src_addr_len, host, NI_MAXHOST,
                        service, NI_MAXSERV, NI_NUMERICSERV);
         if (s == 0)
         {
             printf("short UDP of %ld bytes from %s:%s\n", (long)udp_size, host, service);
         }
         else
         {
             printf("short UDP of %ld bytes from unknown source (getnameinfo: %s)\n", (long)udp_size, gai_strerror(s));
         }
         continue;
      }

      /* Add PSN if requested */
      if (do_add_psn64)
      {
          udp_size += sizeof(uint64_t);
          *((uint64_t*)udp_frame) = psn64;
          psn64++;
      }

      /* Output the UDP packet */
      nwr = sendto(sdo, udp_frame, udp_size, 0, (struct sockaddr *)&si_remote, sizeof(struct sockaddr));
      if (nwr < udp_size) {
          printf("send err\n");
      }

      // Bookkeeping
      framebytes_in_sec += udp_size;

   }

   return 0;
}

