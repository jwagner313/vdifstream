#!/usr/bin/python
'''
Usage: r2dbevdifExtract1PPSOffset.py [-t|--dtsecs <n>] <filename.vdif>

Reads a R2DBE VDIF file and reports the GPS 1PPS to Internal 1PPS offset
that is stored in the VDIF extended user data. The report interval can
be adjusted.

(C) 2018 Jan Wagner
'''

import getopt, math, sys, struct
import astropy.time
from astropy.time import TimeISOT
from astropy.time import Time

verbose = False
header_fmt = '<IIIIIiII'
header_len = struct.calcsize(header_fmt)
dt_scale = 1/256.0e6 # R2DBE clock tick; GPS-fmout = dt_scale * VDIF header signed(edv[3]); positive when GPS 1PPS later than internal 1PPS

vdif_epochs_MJD = [
	# VDIF Epoch 0 = 01/01/2000 UT 0h00min = MJD 51544,   VDIF Epoch 1 is 6 months later,  etc etc
	51544, 51726, 51910, 52091, 52275, 52456, 52640, 52821, 53005, 53187, 53371, 53552, 53736,
	53917, 54101, 54282, 54466, 54648, 54832, 55013, 55197, 55378, 55562, 55743, 55927, 56109,
	56293, 56474, 56658, 56839, 57023, 57204, 57388, 57570, 57754, 57935, 58119, 58300, 58484,
	58665, 58849, 59031, 59215, 59396, 59580, 59761, 59945, 60126, 60310, 60492, 60676, 60857,
	61041, 61222, 61406, 61587, 61771, 61953, 62137, 62318, 62502, 62683, 62867, 63048, 63232,
	63414, 63598, 63779, 63963, 64144, 64328, 64509, 64693, 64875, 65059, 65240, 65424, 65605,
	65789, 65970, 66154, 66336, 66520, 66701, 66885, 67066, 67250, 67431, 67615, 67797, 67981,
	68162, 68346, 68527, 68711, 68892, 69076, 69258, 69442, 69623, 69807, 69988 ]


def usage():
	print ('r2dbevdifExtract1PPSOffset.py [-t|--dtsecs <n>] <filename.vdif>')
	sys.exit(-1)


class TimeYearDayTimeCustom(TimeISOT):
	name = 'yday_fieldsystem'
	subfmts = (('date_hms',	'%Y.%j.%H:%M:%S','{year:d}.{yday:03d}.{hour:02d}:{min:02d}:{sec:02d}'),
		('date_hm', '%Y.%j.%H:%M', '{year:d}.{yday:03d}.{hour:02d}:{min:02d}'),
		('date', '%Y.%j','{year:d}.{yday:03d}'))


def vdif_header_to_time(h):
	mjd = vdif_epochs_MJD[h['ep']]
	mjd += h['sec']/float(24*60*60)
	T = astropy.time.Time(mjd, format='mjd', scale='utc')
	T.format = 'isot'
	T.subformat = 'date_hms'
	Tfs = T.yday_fieldsystem
	return Tfs,T


def vdif_decode_header(data):
	'''
	Decode binary data as a VDIF header
	'''
	s = struct.unpack(header_fmt,data)
	h = {}
	# http://vlbi.org/vdif/docs/VDIF_specification_Release_1.1.1.pdf
	h['I']      = (s[0] & 0x80000000) > 0
	h['L']      = (s[0] & 0x40000000) > 0 
	h['sec']    =  s[0] & 0x3FFFFFFF
	h['ep']     = (s[1] >> 24) & 0x3F
	h['frame']  =  s[1] & 0x00FFFFFF
	h['size']   = (s[2] & 0x00FFFFFF) * 8
	h['nch']    = 2**((s[2] >> 24) & 0x3F)
	h['st']     = '%c%c' % ((s[3]>>8)&0xFF,s[3]&0xFF)
	h['thread'] = (s[3] >> 16) & 0x03FF
	h['bps']    = ((s[3] >> 26) & 0x1F) + 1
	h['C']      = (s[3] & 0x80000000) > 0
	# Extended user data, R2DBE
	h['EDV']    = (s[4] >> 24) & 0x0f
	if h['EDV'] == 2:
		# 0x02 is ALMA https://vlbi.org/vdif/docs/alma-vdif-edv.pdf
		# 0x02 is unfortunately also R2DBE, with different data fields
		# Assume R2DBE here
		h['pps_offset'] = s[5]
	else:
		h['pps_offset'] = float('nan')
	return h


def extract_1pps_offsetinfo(vdiffilename, frame_delta=125000/16, report_delta_secs=10):

	th_sec = {}
	th_offsets = {}
	f = open(vdiffilename, "rb")

	start_offset = None
	start_sec = 0
	end_offset = None
	end_sec = 0

	while True:

		# Read header, skip payload
		header = f.read(header_len)
		if not header:
			break
		h = vdif_decode_header(header)
		if h['size']==0 or h['size']>16*1024*1024:
			print ('Stopping at file offset %d: frame size %d looks bad.' % (f.tell()-header_len,h['size']))
			sys.stdout.write("\033[K")
		skipsize = h['size'] - header_len
		f.seek(skipsize, 1)
		
		# Ignore invalid frames (sometimes timestamp is not correct... in Mark6 rec software...)
		if h['I']:
			continue

		# Report 1PPS offset when the Seconds of a Thread changes
		tID = h['thread']
		if not tID in th_sec:
			th_sec[tID] = h['sec']
			th_offsets[tID] = []

		if (h['sec'] >= (th_sec[tID] + report_delta_secs)):
			Tfs,T = vdif_header_to_time(h)
			N = len(th_offsets[tID])
			if (N > 0):
				mean_offset_s = (sum(th_offsets[tID]) / float(N)) * dt_scale
			else:
				mean_offset_s = float('nan')
			th_sec[tID] = h['sec']
			th_offsets[tID] = []
			if start_offset==None:
				start_offset = mean_offset_s
				start_sec = T
			end_offset = mean_offset_s
			end_sec = T
			rate = (end_offset - start_offset) / ((end_sec - start_sec).sec + 0.1)
			print ('%s/gps-fmout/%+7.4e "mean of %d frames tid#%d, rate now %.6e s/s' % (Tfs,mean_offset_s,N,tID,rate))
		else:
			th_offsets[tID].append(h['pps_offset'])

		# Hop over frame_delta-1 future frames
		f.seek(h['size']*(frame_delta-1), 1)

def run():

	fps = 125000
	report_interval_s = 10
	report_frames = 20
	frame_step = report_interval_s*fps/report_frames

	try:
		opts, args = getopt.getopt(sys.argv[1:], "ht:", ["help","dtsecs="])
	except getopt.GetoptError:
		usage()
		sys.exit(0)

	for opt, optarg in opts:
		if opt in ('-t', '--dtsecs'):
			report_interval_s = int(optarg)
			frame_step = report_interval_s*fps/report_frames
		else:
			usage()
			sys.exit(0)
	if (len(args) != 1):
		usage()

	extract_1pps_offsetinfo(args[0], frame_step, report_interval_s)

run()

