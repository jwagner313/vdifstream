CFLAGS=-Wall -O0 -g -msse3 -D_FILE_OFFSET_BITS=64 -D_LARGEFILE_SOURCE -D_LARGEFILE64_SOURCE -D_GNU_SOURCE -pthread
LDFLAGS=-lpthread -lm -lfftw3f

OBJS = vdifstream vdiftimeUDP vdifsnapshotUDP vdifdiskscatterUDP vdifdiskscatterUDP_gather vdifMonitor vdifforwardUDP
OBJS += vdifsnapshotUDP_MPI

SCRIPTS = vdifcontinuitycheck.py r2dbevdifExtract1PPSOffset.py

all: $(OBJS)

vdifstream: vdifstream.o

vdiftimeUDP: vdiftimeUDP.o

vdifdiskscatterUDP: vdifdiskscatterUDP.o

vdifdiskscatterUDP_gather: vdifdiskscatterUDP_gather.o

vdifMonitor: vdifMonitor.o

vdifforwardUDP: vdifforwardUDP.o

vdifsnapshotUDP_MPI: vdifsnapshotUDP.c
	mpicc -DHAVE_MPI $(CFLAGS) vdifsnapshotUDP.c -o vdifsnapshotUDP_MPI $(LDFLAGS)

clean:
	rm -f *.o $(OBJS)

install: $(OBJS)
	cp -a $(OBJS) $(SCRIPTS) /usr/local/bin/
