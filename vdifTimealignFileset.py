#!/usr/bin/python
import sys, subprocess
from vdifFrame import VDIFFrameHeader, VDIFFrame

VDIF_HDR_LEN = 32
Narg = len(sys.argv) - 1
verbose = False
run_dd = True

def usage():
	print ('')
	print ('Usage:  vdifTimealignFileset.py <file1.vdif> <Mbit/s>  ...  <fileN.vdif> <Mbit/s>')
	print ('')
	print ('Returns the byte offsets required to approximately align the starting times of the files.')
     	print ('At least 2 files must be specified, together with their respective "sample rates" in Mbit/s.')
	print ('')

Nfiles = Narg/2
if (Narg%2 == 1):
	print ('Incomplete arguments: file-datarate pairs expected!')
if (Narg==0) or (Narg%2 == 1) or (Nfiles < 2):
	usage()
	sys.exit(-1)

if verbose:
	print ('Checking the first timestamp of each file...')

files = []
tstart_latest = 0.0

for n in range(Nfiles):
	fname = sys.argv[1+2*n+0]
	fMbps = int(sys.argv[1+2*n+1])
	fid = open(fname, 'rb')
	hdr = VDIFFrameHeader.from_bin(fid.read(VDIF_HDR_LEN))
	fid.close()

	payload_size = hdr.frame_length*8 - VDIF_HDR_LEN
	fps = fMbps*1e6/(8*payload_size)

	tfile = hdr.secs_since_epoch + hdr.data_frame/fps
	if tfile>tstart_latest:
		tstart_latest = tfile

	if verbose:
		print ('File %s : %d Mbps : %d-byte payload : %d frames/sec' % (fname,fMbps,payload_size,fps))
		print ('          timestamp %d sec + %d/%d frames = %.6f sec' % (hdr.secs_since_epoch,hdr.data_frame,fps,tfile))

	fileinfos = {'framesize':hdr.frame_length*8, 'fps':fps, 'starttime':tfile, 'filename':fname}
	files.append(fileinfos)

print ('Determined common start time: ~ %.8f seconds\n' % (tstart_latest))

offsets = []
for n in range(Nfiles):
	fi = files[n]
	fname = fi['filename'] # sys.argv[1+2*n+0]
	tdiff = tstart_latest - fi['starttime']
	pdiff = int(tdiff * fi['fps'])
	bdiff = pdiff * fi['framesize']
	bdiff_initial = bdiff
	fid = open(fname, 'rb')
	while True:
		fid.seek(bdiff, 0)
		hdr = VDIFFrameHeader.from_bin(fid.read(VDIF_HDR_LEN))
		tcurr = hdr.secs_since_epoch + hdr.data_frame/fi['fps']
		# print('file %d : offset %d : sec %d frame %d time %.8f : target %.8f' % (n,bdiff,hdr.secs_since_epoch,hdr.data_frame,tcurr,tstart_latest))
		if (tcurr == tstart_latest):
			break
		elif (tcurr >= tstart_latest):
			bdiff -= fi['framesize']
		elif (tcurr < tstart_latest):
			bdiff += fi['framesize']
	fid.close()
	pdiff = bdiff / fi['framesize']
	offsets.append(bdiff)
	print ('%s : T0=%.12f dT=%.12f : dFrames=%d : start at byte offset=%d, %d byte away from initial guess' % (fi['filename'],fi['starttime'],tdiff,pdiff,bdiff,bdiff-bdiff_initial))

print ('\nRespective commands for a time alignment with "dd":\n')

cmds = []
for n in range(Nfiles):
	#if not(offsets[n] == 0):
	if True:
		fn = files[n]['filename']
		bs = files[n]['framesize']
		skip = offsets[n]/bs
		cmd = 'dd if=%s of=%s.aligned bs=%d skip=%d' % (fn,fn,bs,skip)
		#cmd = 'dd if=%s of=%s.aligned bs=%d skip=%d count=%d' % (fn,fn,bs,skip,fps)
		cmds.append(cmd)
		print (cmd)

if run_dd:
	print ('')
	for cmd in cmds:
		print ('Running %s ...' % (str(cmd)))

		# Python 2.7+
		# subprocess.check_output('dd if=%s of=%s.aligned bs=%d skip=%d' % (fn,fn,bs,skip), stderr=subprocess.STDOUT, shell=True)

		# Python pre 2.7
		# proc = subprocess.Popen(['dd', 'if=%s' % fn, 'of=%s.aligned' % fn, 'bs=%d' % bs, 'skip=%d' % skip], stdout=subprocess.PIPE, shell=True)
		proc = subprocess.Popen(cmd, stdout=subprocess.PIPE, shell=True)
		rc = proc.communicate()

print ('')
