#!/usr/bin/python
#
# Subscribes to multicast UDP on some 10G interface
#
# Based on
# https://pymotw.com/2/socket/multicast.html
# https://stackoverflow.com/questions/603852/how-do-you-udp-multicast-in-python

import socket, struct, sys, time

def createSocket(mcast_group, mcast_port, iface=None):

	grp_address = (mcast_group, mcast_port)

	sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
	sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
	sock.setsockopt(socket.IPPROTO_IP, socket.IP_MULTICAST_LOOP, 1)
	sock.setsockopt(socket.IPPROTO_IP, socket.IP_MULTICAST_TTL, 32)
	sock.setsockopt(socket.SOL_SOCKET,socket.SO_RCVBUF, 9100) # 1 jumboframe
	sock.bind(grp_address)

	mreq = struct.pack('4sL' if iface is None else '4s4s',
		socket.inet_aton(mcast_group),
		socket.INADDR_ANY if iface is None else socket.inet_aton(iface))
	sock.setsockopt(socket.IPPROTO_IP, socket.IP_ADD_MEMBERSHIP, mreq)

	return sock


def run(mcast_group, mcast_port, iface=None):
	sock = createSocket(mcast_group, mcast_port, iface)
	while True:
		print ("Waiting to receive message")
		data, address = sock.recvfrom(9100)
		print ("Received %d bytes from %s" % (len(data),address))
		time.sleep(1)

if __name__ == "__main__":
	if len(sys.argv) != 4:
		print("Usage: multicastSubscribe.py <mcast_ip> <mcast_port> <local_iface_ip>")
		print("  e.g. multicastSubscribe.py 224.3.29.71 4001 192.168.1.2")
		sys.exit(0)
	run(sys.argv[1], int(sys.argv[2]), sys.argv[3])

