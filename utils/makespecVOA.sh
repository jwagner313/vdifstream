#!/bin/bash
#
# Example script to automatically make a spectrum plot of a 4-IF
# recording of VOA data (Japanese hardware for VSI-to-10GbE) that
# were captured with "vdifsnapshotUDP --cpu=2 --offset=0 <x MByte> <filename.voa>"
#

fnin=$1
fnbase=${fnin%.voa}
if [ ! -f $fnbase.voa ]; then
        echo "Cannot find $fnbase.voa (base file name derived from $1)"
        exit -1
fi

# Do conversion from VOA to VDIF (tool is part of https://bitbucket.org/jwagner313/kvnvdiftools)
/mnt/disks/kvnVDIF2VDIF.force1312byte $fnbase.voa $fnbase.vdif YS

# Extract VDIF threads and make spectra (tools are part of DiFX http://cira.ivec.org/dokuwiki/doku.php/difx)
extractSingleVDIFThread $fnbase.vdif $fnbase.T1.vdif 2048 1
extractSingleVDIFThread $fnbase.vdif $fnbase.T2.vdif 2048 2
extractSingleVDIFThread $fnbase.vdif $fnbase.T3.vdif 2048 3
extractSingleVDIFThread $fnbase.vdif $fnbase.T4.vdif 2048 4
m5spec -nopol $fnbase.T1.vdif VDIF_1280-2048-1-2 8192 20000 voa1.m5spec
m5spec -nopol $fnbase.T2.vdif VDIF_1280-2048-1-2 8192 20000 voa2.m5spec
m5spec -nopol $fnbase.T3.vdif VDIF_1280-2048-1-2 8192 20000 voa3.m5spec
m5spec -nopol $fnbase.T4.vdif VDIF_1280-2048-1-2 8192 20000 voa4.m5spec

# Make plot
tmpf=`mktemp`
read -r -d '' FILECONTENT <<ENDFILECONTENT
   plot "voa1.m5spec" using 1:2 with lines title "VOA IF 1", \
        "voa2.m5spec" using 1:2 with lines title "VOA IF 2", \
        "voa3.m5spec" using 1:2 with lines title "VOA IF 3", \
        "voa4.m5spec" using 1:2 with lines title "VOA IF 4"
   set xrange [0:512]
   set yrange [0.1:20]
   set logscale y; replot
   set term postscript landscape enhanced color dashed "Helvetica" 14; set output "$fnbase.m5spec.ps"; replot
ENDFILECONTENT
echo "$FILECONTENT" > $tmpf
gnuplot $tmpf
ls -alh $fnbase.m5spec.ps
rm $tmpf


