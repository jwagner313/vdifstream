#!/bin/bash
#
# Example script to automatically make a spectrum plot of a 4-IF
# recording of FILA10G data (European hardware for VSI-to-10GbE) that
# were captured with "vdifsnapshotUDP --cpu=2 --offset=8 <x MByte> <filename.vdif>"
#

fnin=$1
fnbase=${fnin%.vdif}
if [ ! -f $fnbase.vdif ]; then
        echo "Cannot find $fnbase.vdif (base file name derived from $1)"
        exit -1
fi

# Make spectra (tools are part of DiFX http://cira.ivec.org/dokuwiki/doku.php/difx)
# m5spec -nopol $fnbase.vdif VDIF_1280-8192-4-2 8192 20000 fila10g.m5spec
m5spec -nopol $fnbase.vdif VDIF_5120-8192-4-2 8192 20000 fila10g.m5spec

# Make plot
tmpf=`mktemp`
read -r -d '' FILECONTENT <<ENDFILECONTENT
   plot "fila10g.m5spec" using 1:2 with lines title "FILA10G IF 1", \
        "fila10g.m5spec" using 1:3 with lines title "FILA10G IF 2", \
        "fila10g.m5spec" using 1:4 with lines title "FILA10G IF 3", \
        "fila10g.m5spec" using 1:5 with lines title "FILA10G IF 4"
   set xrange [0:512]
   set yrange [0.1:20]
   set logscale y; replot
   set term postscript landscape enhanced color dashed "Helvetica" 14; set output "$fnbase.m5spec.ps"; replot
ENDFILECONTENT
echo "$FILECONTENT" > $tmpf

gnuplot $tmpf
ls -alh $fnbase.m5spec.ps

rm $tmpf

