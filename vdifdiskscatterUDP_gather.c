#include <arpa/inet.h>
#include <errno.h>
#include <endian.h>
#include <getopt.h>
#include <malloc.h>
#include <memory.h>
#include <netdb.h>
#include <netinet/in.h>
#include <pthread.h>
#include <sched.h>
#include <signal.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <sys/fcntl.h>
#include <time.h>
#include <unistd.h>

#define GATHER_MAGIC       0xFEED7777
#define MAX_SCATTER_FILES  128

void usage(void)
{
    printf("\n"
           " Reassembly counterpart of vdifdiskscatterUDP scattered writer :  Version 1.0\n\n"
           " Usage: vdifdiskscatterUDP_gather <infile 1> <infile 2> ... <infile N> <output file>\n");
}


volatile int m_CtrlC = 0;

void intHandler(int dummy)
{
   printf("Caught Control-C, will try to stop soon...\n");
   m_CtrlC = 1;
}

int main(int argc, char** argv)
{
   int i;
   int fdis[MAX_SCATTER_FILES];
   int fdo;

   uint32_t gatherhdr[4];

   char*  buf = NULL;
   size_t buf_allocsize = 0;
   char*  rwptr = NULL;

   ssize_t nwr;
   ssize_t nrd;
   ssize_t remain;

   int ninfiles;
   int npasses = 0;
   int done = 0;
   size_t nblocks = 0;
   size_t total_bytes = 0;

   struct timeval tv_start;
   struct timeval tv_curr;
   double dT;

   signal(SIGINT, intHandler);

   ninfiles = argc - 2;

   if (ninfiles <= 1)
   {
       usage();
       return -1;
   }

   /* Open all files */
   for (i = 0; i < ninfiles; i++)
   {
       fdis[i] = open(argv[1+i], O_RDONLY|O_LARGEFILE);
       if (fdis[i] == -1)
       {
           fprintf(stderr, "Error opening input file %s : %s\n", argv[1+i], strerror(errno));
           return -1;
       }
   }

   fdo = open(argv[argc-1], O_RDONLY|O_LARGEFILE);
   if (fdo != -1)
   {
       fprintf(stderr, "Error: output file %s already exists! Will not overwrite.\n", argv[argc-1]);
       return -1;
   }
   fdo = open(argv[argc-1], O_LARGEFILE|O_CREAT|O_TRUNC|O_WRONLY, S_IWUSR|S_IRUSR|S_IRGRP|S_IROTH);
   if (fdo == -1)
   {
       fprintf(stderr, "Error: output file %s could not be opened for writing: %s.\n", argv[argc-1], strerror(errno));
       return -1;
   }

   /* Copying loop */
   setbuf(stdout, NULL);
   gettimeofday(&tv_start, NULL);
   while (!done && !m_CtrlC)
   {
       ++npasses;

       for (i = 0; i < ninfiles && !m_CtrlC; i++)
       {
           // Check the header
           nrd = read(fdis[i], &gatherhdr, sizeof(gatherhdr));
           if (nrd < sizeof(gatherhdr))
           {
               fprintf(stdout, "\nEnd of file reached for file %d before block %d.\n", i+1, npasses);
               done = 1;
               break;
           }
           if (gatherhdr[0] != GATHER_MAGIC)
           {
               fprintf(stderr, "Error: file %d block %d did not start with expected magic bytes.\n", i+1, npasses);
               break;
           }
           if (gatherhdr[1] != 1)
           {
               fprintf(stderr, "Error: file %d block %d has version %d. Only 1 supported. Trying anyway.\n", i+1, npasses, gatherhdr[1]);
           }
           if (gatherhdr[2] != buf_allocsize)
           {
              void* b = realloc(buf, gatherhdr[2]);
              buf_allocsize = gatherhdr[2];
              if (b == NULL)
              {
                  fprintf(stderr, "Error: failed to allocate/grow read buffer to %zu bytes.\n", buf_allocsize);
                  done = 1;
                  break;
              }
              buf = (char*)b;
           }

           // Read block from current input file
           remain = (ssize_t)gatherhdr[2];
           rwptr = buf;
           while (remain > 0 && !m_CtrlC)
           {
               nrd = read(fdis[i], rwptr, remain);
               if (nrd <= 0)
               {
                   fprintf(stderr, "Error: failed to read file %d, with %zu bytes of current block remaining.\n", i+1, remain);
                   done = 1;
                   break;
               }
               rwptr += nrd;
               remain -= nrd;
           }

           // Append to output file
           remain = (ssize_t)gatherhdr[2];
           rwptr = buf;
           while (remain > 0 && !m_CtrlC)
           {
               nwr = write(fdo, rwptr, remain);
               if (nwr <= 0)
               {
                   fprintf(stderr, "Error: failed to write remaining %zu bytes of current block.\n", remain);
                   done = 1;
                   break;
               }
               rwptr += nwr;
               remain -= nwr;
           }

           gettimeofday(&tv_curr, NULL);
           total_bytes += (ssize_t)gatherhdr[2];
           dT = (tv_curr.tv_sec + 1e-6*tv_curr.tv_usec) - (tv_start.tv_sec + 1e-6*tv_start.tv_usec);
           fprintf(stdout, "\r Total of %zu blocks, %zu Mbyte, avg copying speed %.0f Mbit/s",
                   ++nblocks, total_bytes/(1024*1024), 8e-6*((double)(total_bytes))/dT );
       }
   }

   fprintf(stdout, "Done.\n");

   return 0;
}
