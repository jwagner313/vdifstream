#include <arpa/inet.h>
#include <errno.h>
#include <endian.h>
#include <getopt.h>
#include <malloc.h>
#include <memory.h>
#include <netdb.h>
#include <netinet/in.h>
#include <pthread.h>
#include <sched.h>
#include <signal.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <sys/fcntl.h>
#include <time.h>
#include <unistd.h>

#define RX_TIMEOUT_SEC       10
#define RX_SOCKET_BUFSIZE_MB 64
#define MAX_RECEIVE_SIZE     65507
#define MIN_RECEIVE_SIZE     1000
#define TRIM_DURING_CAPTURE  1
#define MAX_SCATTER_FILES    128
#define DEFAULT_CFG_FILENAME "vdifdiskscatterUDP.cfg"
#define N_LARGE_BUFS         2

#define WRITE_GATHER_HDR     1  // 1 to insert some headers so that files can be assembled with 'gather' similar to M6 Utils
#define GATHER_MAGIC         0xFEED7777

void usage(void)
{
    printf("\n"
           " Scattered multi-disk writer to capture UDP-encapsulated VDIF :  Version 1.0\n\n"
           " Usage: vdifdiskscatterUDP [--cfg=./%s] [--voa]\n"
           "             <vdif start second to wait for, or 0 to start immediately> <min. duration seconds>\n"
           "             <output file.vdif>\n\n"
           " Args:  --cfg=filename to specify path to configuration file\n"
           "        --voa          to decode Big Endian 'VDIF' headers produced by VOA hardware\n"
           "        start second   the VDIF data second, or 0 for immediate start\n"
           "        duration       the minimum recording length in data seconds\n"
           "        output file    the base file name for scattered parallel writing\n\n", DEFAULT_CFG_FILENAME);
}


int m_thread_verbose = 0;
volatile int m_CtrlC = 0;

// Config
typedef struct cfg_tt {

   // external (.cfg file)
   int udp_rx_cpu;     // CPU to bind UDP receive to
   int udp_port;       // Incoming UDP port
   int nskipfront;     // Number of bytes to discard from start of each UDP packet (e.g., remove 8-byte PSN)
   int outputchunk_MB; // Max. size in MByte to be dumped at once into one output location
   int numoutputs;     // Number of locations (output files) to scatter onto
                       // (UDP snapshot buffer size is thus numoutputs*outputchunk_MB)
   int fd[MAX_SCATTER_FILES];  // File descriptors of output files

   // internal
   size_t buf_len;     // bytes-size of single output writer buffer, depends on actual amount of UDP payload received
   size_t buf_inc;     // bytes-increment per output writer into common buffer, can be 0 if parallel identical-copy write
} cfg_t;


void intHandler(int dummy)
{
   printf("Caught Control-C, will try to stop soon...\n");
   m_CtrlC = 1;
}

void die(const char* fmt, const char* msg)
{
    printf(fmt, msg);
    printf("\n");
    exit(-1);
}

void realtime_init(int pid, int cpu)
{
    cpu_set_t set;
    int rc;

    CPU_ZERO(&set);
    CPU_SET(cpu, &set);
    rc = sched_setaffinity(pid, sizeof(set), &set);
    if (rc < 0)
    {
       printf("sched_setaffinity: could not set CPU affinity (maybe must run as root)?\n");
    }
    else
    {
       printf("Bound process %d to CPU#%d\n", pid, cpu);
    }

    return;
}

int open_udp(const char* port_str)
{
   int sd, c;
   struct addrinfo  hints;
   struct addrinfo* res = 0;
   struct timeval tv_timeout;

   memset(&hints,0,sizeof(hints));
   hints.ai_family   = AF_UNSPEC;
   hints.ai_socktype = SOCK_DGRAM;
   hints.ai_protocol = 0;
   hints.ai_flags    = AI_PASSIVE | AI_ADDRCONFIG;
   getaddrinfo(NULL, port_str, &hints, &res);

   sd = socket(res->ai_family, res->ai_socktype, res->ai_protocol);
   if (bind(sd, res->ai_addr, res->ai_addrlen) == -1)
   {
      die("%s", strerror(errno));
   }
   freeaddrinfo(res);

   tv_timeout.tv_sec  = RX_TIMEOUT_SEC;
   tv_timeout.tv_usec = 0;
   setsockopt(sd, SOL_SOCKET, SO_RCVTIMEO, (char*)&tv_timeout, sizeof(struct timeval));

   c = RX_SOCKET_BUFSIZE_MB*1024*1024;
   setsockopt(sd, SOL_SOCKET, SO_RCVBUF, &c, sizeof(c));

   return sd;
}

// Parse config file and open all output locations
int parse_config_file(FILE* f, cfg_t* cfg, const char* outfilename)
{
   char  line[512];
   char  tmpfilename[4096];
   char* argnames[6] = { "udp_rx_cpu", "udp_port", "nskipfront", "outputchunk_MB", "numoutputs", "output location" };
   int   narg = 0, v;

   memset((void*)cfg, 0, sizeof(cfg_t));

   while (narg < (5 + cfg->numoutputs))
   {

       if ((NULL == fgets(line, sizeof(line), f)) || (strlen(line) <= 1))
       {
          fprintf(stderr, "Error: config file EOF or blank line while expecting to read %s (arg %d)\n",
                  argnames[(narg<=5) ? narg : 5], narg);
          return 0;
       }

       if (line[strlen(line) - 1] == '\n' || line[strlen(line) - 1] == '\r')
       {
          line[strlen(line) - 1] = '\0';
       }
       if (line[strlen(line) - 1] == '\n' || line[strlen(line) - 1] == '\r')
       {
          line[strlen(line) - 1] = '\0';
       }

       if (narg >= 5)
       {
           snprintf(tmpfilename, sizeof(tmpfilename)-1, "%s/%s", line, outfilename);
           // v = open(tmpfilename, O_CREAT|O_APPEND|O_WRONLY, S_IWUSR|S_IRUSR|S_IRGRP|S_IROTH);
           v = open(tmpfilename, O_LARGEFILE|O_CREAT|O_TRUNC|O_WRONLY, S_IWUSR|S_IRUSR|S_IRGRP|S_IROTH);
           fprintf(stderr, "cfg: %2d: %20s = fd=%d fname=%s in %s\n", narg, argnames[(narg<=5) ? narg : 5], v, outfilename, line);
           if (v == -1)
           {
               fprintf(stderr, "cfg: error %s opening output file %s\n", strerror(errno), tmpfilename);
               return 0;
           }
       }
       else
       {
           v = atoi(line);
           fprintf(stderr, "cfg: %2d: %20s = atoi(%s) = %d\n", narg, argnames[(narg<=5) ? narg : 5], line, v);
       }

       switch (narg)
       {
           case 0:
               cfg->udp_rx_cpu = v;
               break;
           case 1:
               cfg->udp_port = v;
               break;
           case 2:
               cfg->nskipfront = v;
               break;
           case 3:
               cfg->outputchunk_MB = v;
               break;
           case 4:
               cfg->numoutputs = v;
               break;
           default:
               cfg->fd[narg-5] = v;
               break;
       }

       narg++;
   }

   return 1;
}


//////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////


// Fill large chunk of memory with VDIF data from the network
// Returns: returns number of bytes read, and sets *udpsize to size of last UDP packet read
size_t capture_to_memory(char* wrbuf, int sd, size_t bufsize, int nskipfront, size_t* udpsize)
{
   size_t  buffilled  = 0;
   size_t  framecount = 0;
   size_t  bufmax     = bufsize - MAX_RECEIVE_SIZE;
   ssize_t nrd;
   char*   minibuf = NULL;

   posix_memalign((void**)&minibuf, 16, nskipfront);
   if (!minibuf)
   {
      printf("Could not allocate %d bytes for mini-buffer.\n", nskipfront);
      return buffilled;
   }

   while (buffilled < bufmax)
   {
      nrd = recv(sd, wrbuf, MAX_RECEIVE_SIZE, 0);
      if (nrd == -1)
      {
          if (errno == EAGAIN)
          {
              printf("\nNo UDP packets received in the last %d seconds...\n", RX_TIMEOUT_SEC);
          }
          else
          {
              printf("error: %s\n", strerror(errno));
              free(minibuf);
              return buffilled;
          }
          break;
      }

      if (nrd < MIN_RECEIVE_SIZE)
      {
          putchar('x');
          continue;
      }

      *udpsize = (size_t)nrd;

#if TRIM_DURING_CAPTURE
      if (nskipfront > 0) // assumes nrd>nskipfront !
      {
          if (buffilled == 0)
          {
              // first packet: discard the first N bytes via 'large' mem move
              memmove(wrbuf, wrbuf+nskipfront, nrd-nskipfront);
              wrbuf -= nskipfront;
          }
          else
          {
              // restore the 'small' data overwritten by previous recvfrom()
              memcpy(wrbuf, minibuf, nskipfront);
          }
          wrbuf     += (nrd-nskipfront);
          buffilled += (nrd-nskipfront);

          // back up end-of-frame data to be overwritten by next recvfrom()
          memcpy(minibuf, wrbuf, nskipfront);
      }
      else
      {
          wrbuf += nrd;
          buffilled += nrd;
      }
#else // !TRIM_DURING_CAPTURE
      wrbuf += nrd;
      buffilled += nrd;
#endif

      framecount++;
      if ((framecount % 10000) == 0)
      {
          putchar('.');
      }
   }
   putchar('\n');

   free(minibuf);
   return buffilled;
}


//////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////


typedef struct worker_arg_tt {
    int    id;
    int    fd;
    char*  buf;
    size_t len;
    size_t nskipfront;
    size_t udpsize;
    cpu_set_t cpuset;
    struct timeval tv_start;
    struct timeval tv_stop;
} worker_arg_t;

worker_arg_t _wargs[MAX_SCATTER_FILES];
pthread_t    _tid[MAX_SCATTER_FILES];


void* scatter_to_file_Worker(void* arg)
{
    size_t  remain;
    ssize_t nwr;
    char*   buf;

    worker_arg_t* args = (worker_arg_t*)arg;
    buf = args->buf;
    remain = args->len;

    gettimeofday(&args->tv_start, NULL);

    if (m_thread_verbose) printf("Output writer %d launched\n", args->id);
    sched_setaffinity(0, sizeof(args->cpuset), &(args->cpuset));

#if WRITE_GATHER_HDR
    {
        uint32_t hdr[4];
        hdr[0] = GATHER_MAGIC;  // magic bytes
        hdr[1] = 1;             // version
        hdr[2] = args->len;     // block size
        hdr[3] = args->udpsize; // packet size (not really used)
        write(args->fd, &hdr, sizeof(hdr));
    }
#endif

#if TRIM_DURING_CAPTURE
    while (remain > 0)
    {
        nwr = write(args->fd, buf, remain);
        if (m_thread_verbose) printf("Wrote %zu bytes, now %zu bytes left.\n", nwr, remain-nwr);
        if (nwr < 0)
        {
           //die("%s", strerror(errno));
           fprintf(stderr, "write error: %s\n", strerror(errno));
        }
        remain -= nwr;
        buf += nwr;
    }
#else
    if (args->nskipfront == 0)
    {
        nwr = write(args->fd, buf, remain);
        if (m_thread_verbose) printf("Wrote %zu bytes, now %zu bytes left.\n", nwr, remain-nwr);
        if (nwr < 0)
        {
           //die("%s", strerror(errno));
           fprintf(stderr, "write error: %s\n", strerror(errno));
        }
        remain -= nwr;
        buf += nwr;
    }
    else while (remain > 0)
    {
         nwr = write(args->fd, buf + args->nskipfront, args->udpsize - args->nskipfront);
         remain -= args->udpsize;
         buf += args->udpsize;
      }
#endif

    //fsync(args->fd);
    gettimeofday(&args->tv_stop, NULL);

    if (m_thread_verbose) printf("Output writer %d done\n", args->id);
    return NULL;
}

int scatter_to_file_start(char* buf, size_t bufsize, size_t buffilled, size_t udpsize, cfg_t* cfg)
{
   int i, c;

   if (buffilled % cfg->numoutputs != 0)
   {
       fprintf(stderr, "Dilemma: the %zu byte input data do not evenly split into %d outputs. "
                       "Will do parallel full-copy write without scattering.", buffilled, cfg->numoutputs);
       cfg->buf_inc = 0;
       cfg->buf_len = buffilled;
   }
   else
   {
       cfg->buf_len = buffilled / cfg->numoutputs;
       cfg->buf_inc = cfg->buf_len;
   }

   for (i=0; i < cfg->numoutputs; i++)
   {
       _wargs[i].id = i;
       _wargs[i].fd = cfg->fd[i];
       _wargs[i].buf = buf;
       _wargs[i].len = cfg->buf_len;
       _wargs[i].nskipfront = cfg->nskipfront;
       _wargs[i].udpsize = udpsize;
       buf += cfg->buf_inc;

       CPU_ZERO(&_wargs[i].cpuset);
       for (c = 2; c < 32; c++)
       {
          if ( (c/2) == (cfg->udp_rx_cpu/2) ) { continue; }
          CPU_SET(c, &_wargs[i].cpuset);
       }

       c = pthread_create(&_tid[i], NULL, &scatter_to_file_Worker, (void*)&_wargs[i]);
       if (c)
       {
           fprintf(stderr, "scatter_to_file: pthread_create error %s\n", strerror(errno));
       }
   }

   return 0;
}

int scatter_to_file_stop(cfg_t* cfg)
{
   int i;
   double Tstart_first = _wargs[0].tv_start.tv_sec + 1e-6 * _wargs[0].tv_start.tv_usec;
   double Tstop_last = 0;

   for (i=0; i < cfg->numoutputs; i++)
   {
       double Tstop;
       pthread_join(_tid[i], NULL);
       Tstop = _wargs[i].tv_stop.tv_sec + 1e-6 * _wargs[i].tv_stop.tv_usec;
       Tstop_last = (Tstop > Tstop_last) ? Tstop : Tstop_last;
   }

   fprintf(stderr, "scatter_to_file: total write throughput %.0f Mbit/s\n", 
           8e-6 * ((double)(cfg->buf_len) * (double)(cfg->numoutputs)) / (Tstop_last - Tstart_first));

   return 0;
}

int scatter_to_file(char* buf, size_t bufsize, size_t buffilled, size_t udpsize, cfg_t* cfg)
{
   scatter_to_file_start(buf, bufsize, buffilled, udpsize, cfg);
   return scatter_to_file_stop(cfg);
}




//////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////

static struct option long_options[] =
    {
        {"voa", no_argument, 0, 'V'},
        {"cfg", required_argument, 0, 'c'},
        {0, 0, 0, 0}
    };


int main(int argc, char** argv)
{
   size_t bufsize;
   size_t buffilled;
   const char* outfilename;
   char   port_str[64];
   char*  bufs[N_LARGE_BUFS];
   size_t udpsize;

   uint32_t startsec;
   uint32_t stopsec;
   uint32_t iter;

   int    sd, c, i;

   FILE*  fcfg;
   cfg_t  cfg;
   char*  cfgfile = DEFAULT_CFG_FILENAME;

   int m_vdif_little_endian = 1;  // 0 to decode VOA data

   /* Command Line Arguments */
   while (1)
   {
      int option_index;
      c = getopt_long(argc, argv, "Vc:", long_options, &option_index);
      if (c == -1) { break; }
      switch (c)
      {
          case 'V':
              m_vdif_little_endian = 0;
              break;
          case 'c':
              cfgfile = strdup(optarg);
              break;
          default:
              usage();
              return -1;
      }
   }
   if ((argc - optind) != 3)
   {
       usage();
       return -1;
   }
   startsec = atoll(argv[optind++]);
   stopsec  = atoll(argv[optind++]);
   outfilename = argv[optind++];

   /* Config file: parse and implicitly open all parallel output files for appending */
   fcfg = fopen(cfgfile, "r");
   if (NULL == fcfg)
   {
       fprintf(stderr, "Error (%s): could not open config file %s.\n", strerror(errno), cfgfile);
       return -1;
   }
   if (!parse_config_file(fcfg, &cfg, outfilename))
   {
      fprintf(stderr, "Please check config file %s.\n", cfgfile);
      return -1;
   }

   /* Init */
   sprintf(port_str, "%d", cfg.udp_port);
   sd = open_udp(port_str);
   if (sd == -1)
   {
      printf("Could not create UDP listener socket.\n");
      return -1;
   }

   bufsize = ((size_t)cfg.outputchunk_MB) * 1048576 * ((size_t)(cfg.numoutputs));
   for (i = 0; i < N_LARGE_BUFS; i++)
   {
       posix_memalign((void**)&bufs[i], 16, bufsize);
       if (!bufs[i])
       {
          printf("Could not allocate %zu bytes for buffer %d of %d.\n", bufsize, i+1, N_LARGE_BUFS);
          return -1;
       }
       else
       {
          printf("Allocated %zu-byte buffer %d of %d.\n", bufsize, i+1, N_LARGE_BUFS);
       }
   }

   printf("Recording VDIF seconds in %d <= T < %d.\n", startsec, stopsec);
   signal(SIGINT, intHandler);
   realtime_init(0, cfg.udp_rx_cpu);
   setbuf(stdout, NULL);

   // Fill initial buffer
   iter = 0;
   printf("Capturing %zu MByte into memory #%d", bufsize/1048576, 1 + (iter % N_LARGE_BUFS));
   buffilled = capture_to_memory(bufs[iter], sd, bufsize, cfg.nskipfront, &udpsize);
   if (buffilled < 0)
   {
      printf("Memory capture ended, no data received.\n");
      return -1;
   }

   while (++iter)
   {
       uint32_t vdif_w0, vdif_sec;
       int do_rec;

       // Previous time stamp; first one in the buffer (don't bother to check *all* frames!)
       vdif_w0 = *((uint32_t*)(bufs[(iter-1) % N_LARGE_BUFS]));
       if (!m_vdif_little_endian)
       {
           vdif_w0 = be32toh(vdif_w0);
       }
       vdif_sec = vdif_w0 & 0x3FFFFFFF;

       if (startsec == 0)
       {
           startsec = vdif_sec;
           stopsec  = vdif_sec + stopsec;
           printf("Adjusted: recording VDIF seconds in %d <= T < %d.\n", startsec, stopsec);
       }

       do_rec = (vdif_sec >= startsec);

       printf("-- Memory %d first VDIF second is %d : ", 1 + (iter-1) % N_LARGE_BUFS, vdif_sec);
       if ((vdif_sec >= stopsec) || m_CtrlC)
       {
           printf("stopping\n");
           break;
       }
       else if (!do_rec)
       {
           printf("waiting (%d seconds to target)\n", startsec - vdif_sec);
       }
       else
       {
           printf("recording (%d seconds remain)\n", stopsec - vdif_sec);
       }

       // Start writing previous data to disk in background
       if (do_rec)
       {
           scatter_to_file_start(bufs[(iter-1) % N_LARGE_BUFS], bufsize, buffilled, udpsize, &cfg);
       }

       // Capture more data to next buffer
       printf("Capturing %zu MByte into memory #%d", bufsize/1048576, 1 + (iter % N_LARGE_BUFS));
       buffilled = capture_to_memory(bufs[iter % N_LARGE_BUFS], sd, bufsize, cfg.nskipfront, &udpsize);
       if (buffilled < 0)
       {
          printf("Memory capture ended, no data received.\n");
          scatter_to_file_stop(&cfg);
          break;
       }

       // Block until earlier write finishes, i.e. before current data is going to be written
       if (do_rec)
       {
           scatter_to_file_stop(&cfg);
       }

   }

   // Done
   for (i = 0; i < N_LARGE_BUFS; i++)
   {
       free(bufs[i]);
   }
   for (i=0; i < cfg.numoutputs; i++)
   {
       printf("Syncing output %d to disk and closing file...\n", i);
       fsync(cfg.fd[i]);
       close(cfg.fd[i]);
   }

   return 0;
}
