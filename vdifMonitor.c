#include <arpa/inet.h>
#include <errno.h>
#include <getopt.h>
#include <malloc.h>
#include <math.h>
#include <memory.h>
#include <netdb.h>
#include <netinet/in.h>
#include <sched.h>
#include <signal.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <sys/fcntl.h>
#include <time.h>
#include <unistd.h>

#include <netinet/ip.h> 
#include <netinet/udp.h> 

#include <fftw3.h>

#define RX_TIMEOUT_SEC       10
#define RX_SOCKET_BUFSIZE_MB 64
#define MAX_RECEIVE_SIZE     ((size_t)65507)
#define MIN_RECEIVE_SIZE     1000
#define TRIM_DURING_CAPTURE  1

#define MIN(X, Y) (((X) < (Y)) ? (X) : (Y)) 

// test stream:
// ./vdifstream --log2nchan=0 --size=8192 127.0.0.1 46123 4096 /data1/eht2017/lo-band/slot12/e17e11_Ar_101-0322.vdif
// ./vdifMonitor -s 64M -o 8 lo 46123

///////////////////////////////////////////////////////////////////////////////////////////////////////////////

static struct option long_options[] =
{
    {"size",      required_argument, 0, 's'},
    {"offset",    required_argument, 0, 'o'},
    {"cpu",       required_argument, 0, 'c'},
    {"thread",    required_argument, 0, 't'},
    {0, 0, 0, 0}
};

static uint32_t vdif_epoch_MJDs[] = {
    // VDIF Epoch 0 = 01/01/2000 UT 0h00min = MJD 51544,   VDIF Epoch 1 is 6 months later,  etc etc
    51544, 51726, 51910, 52091, 52275, 52456, 52640, 52821, 53005, 53187, 53371, 53552, 53736,
    53917, 54101, 54282, 54466, 54648, 54832, 55013, 55197, 55378, 55562, 55743, 55927, 56109,
    56293, 56474, 56658, 56839, 57023, 57204, 57388, 57570, 57754, 57935, 58119, 58300, 58484,
    58665, 58849, 59031, 59215, 59396, 59580, 59761, 59945, 60126, 60310, 60492, 60676, 60857,
    61041, 61222, 61406, 61587, 61771, 61953, 62137, 62318, 62502, 62683, 62867, 63048, 63232,
    63414, 63598, 63779, 63963, 64144, 64328, 64509, 64693, 64875, 65059, 65240, 65424, 65605,
    65789, 65970, 66154, 66336, 66520, 66701, 66885, 67066, 67250, 67431, 67615, 67797, 67981,
    68162, 68346, 68527, 68711, 68892, 69076, 69258, 69442, 69623, 69807, 69988
};

typedef struct capture_stats_tt {
    float rate_Mbps;       // estimated data rate in Mbit/s
    float timeoffset_sec;  // offset between VDIF time and computer time
    size_t nchan;          // number of VDIF channels
    size_t nbit;           // for now always 2-bit, 4-level
    size_t nlevel;
    uint32_t** counts;     // counts[nchan][nlevels]
} capture_stats_t;

///////////////////////////////////////////////////////////////////////////////////////////////////////////////

void vdif_time_to_tm(uint8_t vdifepoch, uint32_t frame_sec, struct tm* utc_vdif);
int open_raw_socket(const char* iface);
void realtime_init(int cpu);
ssize_t capture_filtered(int sd, const int port, const int vdif_thread, const int frame_offset, unsigned char* buf, const size_t bufsize, capture_stats_t* stats);
double diff_vdif_systemtime(int vdifepoch, uint32_t frame_sec);

///////////////////////////////////////////////////////////////////////////////////////////////////////////////

void usage(void)
{
    printf("\nUsage: vdifMonitor [-s|--size <capturesize>] [-o|--offset <length of PSN if any>]\n"
           "                   [-c|--cpu <core_nr>] [-t|--thread <VDIF thread ID>] <eth_iface> <udp port>\n\n"
           " capturesize   size in MByte of every snapshot to use for checking sample data\n"
           " offset        length of packet sequence number (0 or 4 or 8 byte)\n"
           " core_nr       bind the data capture to a specific CPU core\n"
           " thread ID     capture data from only the given VIDF thread ID\n\n");
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////

void die(const char* fmt, const char* msg)
{
    printf(fmt, msg);
    printf("\n");
    exit(1);
}

volatile int m_CtrlC = 0;
void intHandler(int dummy)
{
   printf("Caught Control-C, will try to stop soon...\n");
   m_CtrlC = 1;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////

void realtime_init(int cpu)
{
    cpu_set_t set;
    int rc;

    CPU_ZERO(&set);
    CPU_SET(cpu, &set);
    rc = sched_setaffinity(0, sizeof(set), &set);
    if (rc < 0)
    {
       printf("sched_setaffinity: could not set CPU affinity (maybe must run as root)?\n");
    }
    else
    {
       printf("Bound to CPU#%d\n", cpu);
    }

    return;
}

int open_raw_socket(const char* iface)
{
    int sd;
    sd = socket(AF_INET, SOCK_RAW, IPPROTO_UDP);
    if (sd == -1) {
        printf("SOCK_RAW open failed: %s\n", strerror(errno));
        return -1;
    }
    if (setsockopt(sd, SOL_SOCKET, SO_BINDTODEVICE, iface, strlen(iface)) == -1) {
        printf("SO_BINDTODEVICE to %s failed: %s\n", iface, strerror(errno));
        return -1;
    }
    return sd;
}

void vdif_time_to_tm(uint8_t vdifepoch, uint32_t frame_sec, struct tm* utc_vdif)
{
    uint32_t MJD, yearMJD;
    uint32_t epdays;
    double rem;
    int doy, year, evenvdifepoch;

    if (vdifepoch >= sizeof(vdif_epoch_MJDs)/sizeof(uint32_t))
    {
        vdifepoch = 0;
    }
    evenvdifepoch = 2*((int)(vdifepoch/2)); // ref.ep. 0, 2, 4, ... are the first day of a year (01/01/20xx)

    rem = (double)frame_sec;

    epdays = (uint32_t)(rem / (24.0*60.0*60.0));
    rem = rem - epdays*24.0*60.0*60.0;

    utc_vdif->tm_hour = (int)(rem / (60.0*60.0));
    rem = rem - utc_vdif->tm_hour*60.0*60.0;

    utc_vdif->tm_min = (int)(rem / 60.0);
    rem = rem - utc_vdif->tm_min*60.0;

    utc_vdif->tm_sec = (int)rem;

    MJD     = vdif_epoch_MJDs[vdifepoch] + epdays;     // 01/01-or-06/20xx + days
    yearMJD = vdif_epoch_MJDs[evenvdifepoch];          // 01/01/20xx
    doy     = (MJD - yearMJD) + 1;
    year    = 2000 + ((int)(vdifepoch/2));             // ref.ep. 0 is 01/01/2000, ref.ep. 2 is 01/01/2001, ...

    utc_vdif->tm_year = year - 1900;  // years since 1900
    utc_vdif->tm_yday = doy - 1;      // days since Jan 01 (0-365)
}

double diff_vdif_systemtime(int vdifepoch, uint32_t frame_sec)
{
    time_t    now;
    struct tm utc_pc;
    struct tm utc_vdif;
    struct timeval tv_curr;
    double dt, curr_sec_fraction;

    gettimeofday(&tv_curr, NULL);
    time(&now);
    curr_sec_fraction = 1e-6*tv_curr.tv_usec;

    vdif_time_to_tm(vdifepoch, frame_sec, &utc_vdif);
    gmtime_r(&now, &utc_pc);

    dt = ((double)utc_vdif.tm_sec + 60.0*utc_vdif.tm_min + 3600.0*utc_vdif.tm_hour)
         - (curr_sec_fraction + (double)utc_pc.tm_sec + 60.0*utc_pc.tm_min + 3600.0*utc_pc.tm_hour);

    return dt;
}

ssize_t capture_filtered(int sd, const int port, const int vdif_thread, const int frame_offset, unsigned char* buf, const size_t bufsize, capture_stats_t* stats)
{
    int port_net = htons(port);

    unsigned char* packet = (unsigned char *)malloc(MAX_RECEIVE_SIZE);
    struct iphdr *iph = (struct iphdr*)packet;
    struct udphdr *udp;

    unsigned char* vdif;
    unsigned char* vdif_payload;
    uint32_t w32[4];
    uint32_t frame_no_prev = 0;
    size_t n = 0;
    size_t ncaptured = 0, nmissed = 0;

    int did_timecheck = 0;
    int did_frameno = 0;
    int triggered = 0;

    // Reset
    memset(buf, 0, bufsize);
    stats->timeoffset_sec = 0;
    stats->rate_Mbps = 0;

    // Start capture
    struct timeval tv_start, tv_stop;
    gettimeofday(&tv_start, NULL);
    //printf(" n=%zu bufsize_limit=%zu ", n, bufsize - MAX_RECEIVE_SIZE);
    while (n < (bufsize - MAX_RECEIVE_SIZE)) {
        uint32_t frame_no, frame_sec, frame_thread_id;
        size_t nrd, frame_payload_len;

        // Low-level Eth frame capture
        nrd = recv(sd, packet, MAX_RECEIVE_SIZE, 0);
        if (nrd < 0) {
            printf("recv() failed: %s\n", strerror(errno));
            return -1;
        }

        // Look for UDP (proto 17), given destination port
        if (iph->protocol != 17) {
            continue;
        }
        udp = (struct udphdr*)(packet + iph->ihl*4);
        if (udp->dest != port_net) {
            printf("skipping UDP packet to dest port %d\n", ntohs(udp->dest));
            continue;
        }

        // Look at VDIF header
        vdif = packet + iph->ihl*4 + sizeof(struct udphdr) + frame_offset;
        vdif_payload = vdif + 8*4; // assuming not 'Legacy' frame
        if (0) {
            int m;
            printf("vdif hdr: ");
            for (m=0; m<32; m++) {
                printf("%02X ", vdif[m]);
            }
            printf("\n");
        }
        w32[0] = vdif[0] | ((uint32_t)vdif[1])<<8 | ((uint32_t)vdif[2])<<16 | ((uint32_t)vdif[3])<<24; 
        w32[1] = vdif[4] | ((uint32_t)vdif[5])<<8 | ((uint32_t)vdif[6])<<16 | ((uint32_t)vdif[7])<<24; 
        w32[2] = vdif[8] | ((uint32_t)vdif[9])<<8 | ((uint32_t)vdif[10])<<16 | ((uint32_t)vdif[11])<<24; 
        w32[3] = vdif[12] | ((uint32_t)vdif[13])<<8 | ((uint32_t)vdif[14])<<16 | ((uint32_t)vdif[15])<<24; 
        frame_sec = w32[0] & 0x3FFFFFFF;
        frame_no  = w32[1] & 0x00FFFFFF;
        frame_thread_id = (w32[3] >> 16) & 0x3F;
        frame_payload_len = 8*(w32[2] & 0x00FFFFFF);
        if ((vdif_thread >= 0) && (frame_thread_id != vdif_thread)) {
            printf("skipping VDIF frame from thread %d\n", frame_thread_id);
        }
        if (stats->nchan == 0) {
            stats->nchan = 1 << ((w32[2] >> 24)&0b11111);
        }

        // Ignore all frames until the seconds change
        if (frame_no == 0) {
            triggered = 1;
        }
        if (!triggered) {
            continue;
        }

        // Compare VDIF and system time, but only for the first frame of a second (to keep things easier)
        if (!did_timecheck && (frame_no == 0)) {
            int vdifepoch = (w32[1] >> 24) & 0x3F;
            stats->timeoffset_sec = diff_vdif_systemtime(vdifepoch, frame_sec);
            did_timecheck = 1;
        }

        // Grab the VDIF frame payload, discard header
        if (!did_frameno) {
            frame_no_prev = frame_no;
        }
        if (frame_no != (frame_no_prev + 1)) {
            if (frame_no <= frame_no_prev) {
                // Roll-over of frame nr, assuming here that no frames went missing between last frame of previous second and current frame
            } else {
                // Missing frames; assume all frames are equal size
                n += frame_payload_len * (frame_no - frame_no_prev);
                nmissed += (frame_no - frame_no_prev) - 1;
            }
        }
        if ((n + frame_payload_len) > bufsize) {
            break;
        } else {
            memcpy(buf+n, vdif_payload, frame_payload_len);
        }

        frame_no_prev = frame_no;
        n += frame_payload_len;
        ncaptured++;
    }

    gettimeofday(&tv_stop, NULL);
    stats->rate_Mbps = (n*8.0)*1e-6 / (tv_stop.tv_sec - tv_start.tv_sec + 1e-6*(tv_stop.tv_usec - tv_start.tv_usec));

    //printf(" n=%zu dT=%f ", n,  (tv_stop.tv_sec - tv_start.tv_sec + 1e-6*(tv_stop.tv_usec - tv_start.tv_usec)));
    printf("Captured %zu frames, missed %zu frames\n", ncaptured, nmissed);

    return n;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////

int vdif_raw_bitstat_2bit(const unsigned char* raw8bit, size_t nbytes, size_t nchan, capture_stats_t* st)
{
    const int nbit = 2;
    const uint32_t mask = 0x03;
    size_t nsamps_processed = 0;
    size_t nbyte_processed = 0;
    //size_t nchan = st->nchan;
    st->nbit = nbit;
    st->nlevel = 4;
    int i;

    if (st->counts == NULL) {
        st->counts = malloc(nchan*sizeof(uint32_t*));
        if (st->counts == NULL) {
            printf("Error: alloc of %zu bytes for stcount[] ptrs failed!", nchan*sizeof(uint32_t*));
            exit(1);
        }
        for (i = 0; i < nchan; i++) {
            st->counts[i] = malloc(st->nlevel*sizeof(uint32_t));
        }
        // TODO: what if stream 'nchan' changed...
    } else {
        for (i = 0; i < nchan; i++) {
            memset(st->counts[i], 0, st->nlevel*sizeof(uint32_t));
        }
    }

    while ((nbyte_processed + sizeof(uint32_t)) < nbytes) {

        size_t n = nbyte_processed;
        uint32_t currsamples = raw8bit[n+3] | ((uint32_t)raw8bit[n+2])<<8 | ((uint32_t)raw8bit[n+1])<<16 | ((uint32_t)raw8bit[n+0])<<24;

        int nused = 0;
        while (nused < (32/nbit)) {
            int ch = nused % st->nchan;
            uint32_t sample = currsamples & mask;
            st->counts[ch][sample]++;
            currsamples = currsamples >> nbit;
            nused++;
        }
        nsamps_processed += nused;
        nbyte_processed += sizeof(uint32_t);
    }

    printf("Sample value distribution in %zu channels:\n", nchan);
    for (i = 0; i < nchan; i++) {
        size_t samps_per_ch = nsamps_processed / nchan;	
        int lev;
        printf("ch %2d : ", i);
        for (lev = 0; lev < st->nlevel; lev++) {
            printf("%6.2f ", (100.0f*st->counts[i][lev])/((double)samps_per_ch));
        }
        printf("\n");
    }

    return 0;
}

static const float lut2bit[4] = {-3.3359f, -1.0f, +1.0f, +3.3359f};

int decode_2bit_1channel(const unsigned char* src, float* dst, size_t Nbyte)
{
    float* dst_initial = dst;
    const unsigned char* src_end = src + Nbyte;
    for (; src < src_end ; src+=4, dst+=16) {
        // 32-bit block unpacking
        unsigned char a = src[0], b = src[1], c=src[2], d=src[3];
        dst[ 0] = lut2bit[(a >> 0) & 3];
        dst[ 1] = lut2bit[(a >> 2) & 3];
        dst[ 2] = lut2bit[(a >> 4) & 3];
        dst[ 3] = lut2bit[(a >> 6) & 3];
        dst[ 4] = lut2bit[(b >> 0) & 3];
        dst[ 5] = lut2bit[(b >> 2) & 3];
        dst[ 6] = lut2bit[(b >> 4) & 3];
        dst[ 7] = lut2bit[(b >> 6) & 3];
        dst[ 8] = lut2bit[(c >> 0) & 3];
        dst[ 9] = lut2bit[(c >> 2) & 3];
        dst[10] = lut2bit[(c >> 4) & 3];
        dst[11] = lut2bit[(c >> 6) & 3];
        dst[12] = lut2bit[(d >> 0) & 3];
        dst[13] = lut2bit[(d >> 2) & 3];
        dst[14] = lut2bit[(d >> 4) & 3];
        dst[15] = lut2bit[(d >> 6) & 3];
    }
    if (0) {
        FILE* f = fopen("/tmp/raw_decoded.txt", "w");
        int i;
        for (i=0; i<(32768); i++) {
            //fprintf(f, "%d %f\n", i, dst_initial[i]);
            fprintf(f, "%f\n", dst_initial[i]);
        }
        fclose(f);
    }
    return 0;
}

#define C32ABS(x) sqrtf(x[0]*x[0]+x[1]*x[1])

void add_c32_I(fftwf_complex* __restrict__ acc, const fftwf_complex* __restrict__ b, const int N)
{
    const int unroll = 8;
    int i;
    for (i = 0; i < (N-unroll); i += unroll) {
        int j;
        fftwf_complex* __restrict__ dst = acc + i;
        const fftwf_complex* __restrict__ src = b + i;
        for (j = 0; j < unroll; j++) {
            dst[j][0] += src[j][0];
            dst[j][1] += src[j][1];
        } 
    }
    for ( ; i < N; i++) {
        acc[i][0] += b[i][0];
        acc[i][1] += b[i][1];
    }
}

void add_c32_abs_I(float* __restrict__ acc, const fftwf_complex* __restrict__ b, const int N)
{
    const int unroll = 8;
    int i;
    for (i = 0; i < (N-unroll); i += unroll) {
        int j;
        float* __restrict__ dst = acc + i;
        const fftwf_complex* __restrict__ src = b + i;
        for (j = 0; j < unroll; j++) {
            dst[j] += C32ABS(src[j]);
        } 
    }
    for ( ; i < N; i++) {
        acc[i] += C32ABS(b[i]);
    }
}

int spectral_checks(float* raw, size_t N, double fs, double Tint_sec, double tonefreq_Hz, int Ldft)
{
    size_t Nused = 0;
    int nint = floor(fs*Tint_sec/(double)Ldft);
    const size_t integ_samples = Ldft*(size_t)nint;
    double Tint = ((double)(nint*Ldft))/fs;
    double pcbin_float = Ldft*tonefreq_Hz/fs;
    int pcbin = (int)(pcbin_float + 0.5);
    nint = integ_samples / Ldft;
    int do_pcal = 1;

    if (pcbin_float != pcbin) {
        printf("Warning: tone bin %f not an integer! Not extracting tone.\n", pcbin_float);
        do_pcal = 0;
    } else {
        printf("Coherence of pcal tone:\n");
    }

    fftwf_complex* spectrum = fftwf_malloc(sizeof(fftwf_complex)*(Ldft/2+1));
    fftwf_complex* specinteg = fftwf_malloc(sizeof(fftwf_complex)*(Ldft/2+1));
    float* specabsinteg = malloc(sizeof(float)*(Ldft/2+1));
    fftwf_plan fft = fftwf_plan_dft_r2c_1d(Ldft, raw, spectrum, FFTW_ESTIMATE);
    memset(specabsinteg, 0, sizeof(float)*(Ldft/2+1));

    while (Nused  < N) {

        float pcamp = 0;
        memset((void*)specinteg, 0, sizeof(fftwf_complex)*(Ldft/2+1));

        int iter = 0;
        while (iter < nint) {
            ssize_t Nleft = N - Nused;
            if (Nleft < Ldft) {
                Nused = N;
                break;
            }

            fftwf_execute_dft_r2c(fft, raw + Nused, spectrum);
            pcamp += C32ABS(spectrum[pcbin]);
            add_c32_I(specinteg, (const fftwf_complex*)spectrum, Ldft/2);
            add_c32_abs_I(specabsinteg, (const fftwf_complex*)spectrum, Ldft/2);
            Nused += Ldft;
            iter++;
        }

        float pctone_avg = C32ABS(specinteg[pcbin]);
        if (do_pcal) {
            float pctone_coh = pctone_avg / pcamp;
            float pctone_phase_deg = 57.2957795f * atan2f(specinteg[pcbin][1], specinteg[pcbin][0]); // assuming {Re,Im} FFTW packing
            printf("  %.4fs %.4f %+7.2f\n", (Nused-integ_samples/2)/fs, pctone_coh, pctone_phase_deg);
        }
    }

    if (1) {
        FILE* f = fopen("/tmp/spec_tmp.txt", "w");
        if (f == NULL) {
            perror("fopen(/tmp/spec_tmp.txt)");
        } else {
            int i;
            for (i=0; i<Ldft/2; i++) {
//                fprintf(f, "%f %f\n", (i*fs*1e-6)/((float)Ldft), specabsinteg[i]);
                fprintf(f, "%f %f\n", (i*fs*1e-6)/((float)Ldft), C32ABS(spectrum[i]));
            }
            fclose(f);
            rename("/tmp/spec_tmp.txt", "/tmp/spec.txt");
        }
    }

    fftwf_destroy_plan(fft);
    fftwf_free(spectrum);
    fftwf_free(specinteg);
    free(specabsinteg);
    return 0;
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////

int main(int argc, char** argv)
{
    size_t bufsize = 1 * 1048576;
    int    nskipfront = 0;
    int    thread = -1;
    int    sd, port;
    const char* iface;
    unsigned char* buf = NULL;
    float* buf_f32 = NULL;
    capture_stats_t st;

    setbuf(stdout, NULL);

    /* Optional cmd line arguments */
    while (1) {
        int option_index, c;
        c = getopt_long(argc, argv, "s:o:c:t:", long_options, &option_index);
        if (c == -1) { break; }
        switch (c) {
            case 's':
                bufsize = atol(optarg) * 1048576;
                break;
            case 'o':
                nskipfront = atoi(optarg);
                break;
            case 'c':
                realtime_init(atoi(optarg));
                break;
            case 't':
                thread = atoi(optarg);
                break;
            default:
                usage();
                return -1;
        }
    }

    /* Required cmd line arguments */
    if ((argc - optind) != 2) {
        usage();
        return -1;
    }
    iface = argv[optind++];
    port = atoi(argv[optind++]);

    /* Work buffer */
    posix_memalign((void**)&buf, 4096, bufsize);
    posix_memalign((void**)&buf_f32, 4096, bufsize*sizeof(float)*8); // allocate to max in case of 1-bit samples
    if (!buf) {
        printf("Could not allocate capture buffer of %zu bytes.\n", bufsize);
        exit(2);
    }
    if (!buf_f32) {
        printf("Could not allocate decoded sample (float32) buffer of %zu bytes.\n", bufsize*sizeof(float));
        exit(2);
    }

    /* Raw socket */
    sd = open_raw_socket(iface);
    if (sd == -1) {
        exit(3);
    }

    /* Capture one buffer at a time and process it */
    signal(SIGINT, intHandler);
    memset(&st, 0, sizeof(capture_stats_t));
    while (!m_CtrlC) {

        printf("Next data capture...\n");
        capture_filtered(sd, port, thread, nskipfront, buf, bufsize, &st);
        if (st.nchan != 1) {
            printf("Data had an unsupported nr of channels of %zu, skipping\n", st.nchan);
            continue;
        }
        printf("Current rate %.3f MBps, time offset %.6f seconds\n", st.rate_Mbps, st.timeoffset_sec);	

        // Analyze
        vdif_raw_bitstat_2bit(buf, MIN(bufsize,1024*1024), st.nchan, &st);
        if (st.nchan == 1) {
            size_t Nfloat = (bufsize*8)/st.nbit;
            decode_2bit_1channel(buf, buf_f32, bufsize);
            spectral_checks(buf_f32, Nfloat, 4096e6, 0.010, 525e6, 262144); // R2DBE 4096 Ms/s, APEX PCal tone at 525.0 MHz
        }
    }

    return 0;
}
